export class Topic {
    constructor(
        public title: string,
        public anchor: string,
        public content: string,
        public goal: string
    ) { }
}