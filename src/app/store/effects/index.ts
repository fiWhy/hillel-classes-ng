import { MenuEffects } from './menu.effects';
import { MaterialEffects } from './material.effects';

export const effects = [
    MenuEffects,
    MaterialEffects
];
